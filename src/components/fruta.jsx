import { Component } from "react";

class Fruta extends Component {
  render() {
    return <li>{this.props.fruit}</li>;
  }
}

export default Fruta;
