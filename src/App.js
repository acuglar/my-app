import React from 'react';
import Fruta from './components/fruta'

class App extends React.Component {
  fruits = [
    { name: "banana", color: "yellow", price: 2 },
    { name: "cherry", color: "red", price: 3 },
    { name: "strawberry", color: "red", price: 4 },
  ]

  render() {
    return (
      <div className="App" >
        <div id="all fruit names">
          <p>All fruit names:</p>
          <ul>
            {this.fruits.map(cur => <Fruta fruit={cur.name} />)}
          </ul>
        </div>
        <div id="red fruit names">
          <p>Red fruit names:</p>
          <ul>
            {this.fruits
              .filter(cur => cur.color === 'red')
              .map(cur => <Fruta fruit={cur.name} />)}
          </ul>
        </div>
        <div id="total">
          <p>Total:</p>
          <ul>
            {this.fruits.reduce((acc, cur) => acc + cur.price, 0)}
          </ul>
        </div>
      </div >
    )
  };
}

export default App;